# v21.03

Publicly released: 2016-01-03 `╰[ ՞(oo)՞ ]╯`

Based off Ultimaker's 15.02.01 version of Cura

## Firmware

    Mini (v1.1.0.11)
        Workaround a rare hardware-specific bug that occurs during probing where the right side washers are never measured

## Quickprint Profiles

    New experimental materials:
        IC3D: ABS
    Small enhancements to quite a few profiles
    Corrected naming or spelling of a few manufacturers and filaments
    Add URLs to some newer materials
    Reclassify a few materials as experimental:
        CC-Products: Laybrick, Laywood
        Taulman: Nylon 618

# v21.02

Publicly released: 2016-11-21 `⋋╏ ✿ ͡° .ں. ͡° ✿ ╏⋌`

Based off Ultimaker's 15.02.01 version of Cura

## Firmware

    Mini (v1.1.0.10) Standard/Flexy
        Improve M190: use narrower window (<1°C instead of 5°C) while cooling down to a target bed temperature

## Quickprint Profiles

    Updated part removal temperatures for all PLAs
    New supported materials:
        colorFabb: HT-5300
        Polymaker: PC-Max
        twoBEars: BioFila Silk & Linen

# v21

Publicly released: 2016-10-04 `\(ʘᗩʘ)/`

Based off Ultimaker's 15.02.01 version of Cura
## New

    Support for the MOARstruder Tool Head (with a 1.2mm diameter nozzle) on TAZ 5 & 6

## Fixed

    [Win 10] crash that occurred immediately after displaying the splash screen

## Firmware

    Mini (v1.1.0.9) Standard/Flexy
        Backwards compatible on all existing Mini's
        Based on newer upstream version (1.1)
        Failed probe detection & re-wipe procedure (just like TAZ 6)
            Prevents damage to PEI print surface
        Fix for M109/M190 (don't wait for cooling)
        Y-axis homes to opposite direction, but keeps same positioning of printed models

    TAZ 6 (v1.0.2.20) Standard/Flexy/Dually/FlexyDually/MOAR
        Backwards compatible on all existing TAZ 6's
        Raise tool head after failed probing to allow easy nozzle cleaning
        Improve labeling for thermal errors
        Display status of all endstops with M119

    TAZ 5 (v1.0.0.2) MOARstruder

    TAZ 5 (v1.0.0.2) Flexy
        Remove artificial nozzle temperature restriction, can now use full range of hardware

## Quickprint Profiles

    32 supported materials on Single Extruder for each TAZ 6 and Mini
    Experimental material:
        twoBEars: Biofila Linen
    MOARstruder
        8 supported materials
        Several spiral vase profiles
        7 experimental materials

# v20

Publicly released: 2016-08-08 `ᕦ( * ▃ * )ᕤ`

Based off Ultimaker's 15.02.01 version of Cura
## New

    Print window displays:
        Timestamps when print jobs start and end
        Completed print duration time
    Category for experimental profiles within Quickprint

## Fixed

    Firmware install
        Process no longer auto-starts
        Changing tool head on a TAZ 6 no longer skips firmware update step

## Quickprint Profiles

    Refined support settings
    New materials:
        Polymaker: PLA Polylite
        Verbatim: PLA
    Experimental materials:
        colorFabb: HT-5300, BrassFill
        Polymaker: PC-MAX, PolyFlex
        NinjaTek: Armadillo
        twoBEars: BioFila Silk
        B-Pet
    M109 & M190s have been swapped to use R### instead of S###
    Allow printing a single material while using dual and flexydual on TAZ6
    Fix a few profiles which accidentally included 'cut off object bottom'

# v19

Publicly released: 2016-04-12 `| ◔ ╭ ͟ʖ╮ ◔ |`

Based off Ultimaker's 15.02.01 version of Cura
## New

    TAZ 6
        Single Extruder v2.1
        FlexyStruder v2
        Dual Extruder v2
        FlexyDually v2
    Prints that fail probing procedure are automatically cancelled
    Updated "first print" Rocktopus model

## Fixed

    [GNU/Linux] 250000 baud rate
    [Windows] While printing from USB, a crash that can occur after 3+ hours of printing
    Robustness of importing profile from G-Code
    Shortcut to open print window <CTRL> + <P>
    Machine settings window indicates which USB ports are unavailable
    Plugins can be correctly disabled
    Prevent rare issue where printing an object at maximum height on a LulzBot Mini could scar top portion of the print
    During cool down after a completed print, prevent situation where bed does not move forward immediately after reaching target temperature

## Firmware

    TAZ 6
        Thermal runaway detection
        Automatic Self-leveling
        Failed probe detection & re-wipe procedure
        Z offset adjustment menu
        Baud rate upped to 250000
        Prevent rare case where SD prints are interrupted

    Mini, TAZ 4 & TAZ 5
        Different supplier of cooling fans was introduced mid-production stream
        New firmware is backwards compatible meaning that it works with both types of fans
        Print quality is slightly improved in a few cases
        No functional changes
        Updated for all LulzBot Hexagon (v2) hotends:
            Standard
            FlexyStruder
            Dual Extruder
            FlexyDually

## Quickprint Profiles

    Significant overhaul of start/end G-Code on all Mini profiles
    New filaments added. There are now 33 materials available in quickprint. Under the hood there are over 300 integrated profiles for LulzBot printers!
        colorFabb: nGen, corkFill, PLA/PHA
        Proto-pasta: Aromatic Coffee PLA, High Temp PLA, PC-ABS
        NinjaTek: Cheetah
        Taulman: Alloy 910, n-vent
        CC-Products: Laywoo-D3
        New combinations of materials for Dual and FlexyDual tool heads

# v18

Publicly released: 2016-01-04  `(͡◕‿◕͡ )`

Based off Ultimaker's 15.02.01 version of Cura
## New

    Pause
        While printing over USB, any ongoing print can be halted. This will allow you to:
            Change filament color
            Swap spools before running out of filament
            Embed an object inside the print
            Clear partial nozzle clog
            Inspect internal structure of print
            Prime the nozzle at the start of a print
        Feed filament by hand or click the extrude/retract buttons
    More detailed print progress indicator
        Title bar of print window shows a percentage as well as current Z-height

## Fixed

    [Windows] several installation problems
    [Fedora] separated into 64 bit and 32 bit builds
    Notification messages are shortened instead of not displayed when there is too much text
    A significant number of minor cosmetic and labeling items
    Slow down filament feed rate for Flexystruders (say that three times fast) when clicking extrude/retract to prevent jamming

## Quickprint Profiles

Several new materials have been added:

    colorFabb:
        XT
        Bronzefill
        Copperfill
        Woodfill
        Bamboofill
    ChromaStrand: INOVA-1800

# v17

Publicly released: 2015-09-26 `ᕕ(⌐■_■)ᕗ ♪♬`

Based off Ultimaker's 15.02.01 version of Cura
## New

    Quickprint has lots more materials (other than ABS/HIPS/PLA)
        So many materials that we had to start using drop down menus!
        Categorization of material by difficulty
            First print
            Beginner
            Intermediate
            Advanced (may require bed preparation)
            Expert (may require hardware changes such as a different tool head)
        Just for single extruder tool heads so far...
    Easy way to tweak/modify a quickprint profile
        Swapping "quickprint" --> "full settings" mode asks if you want to keep settings
        See what settings are being used then make a few tweaks of your own
    Reworked method for configuring new printers (with shiny pictures)
        Tool head selection
        v2 LulzBot Hexagon tool heads with All Metal Hot End
        v1 Budaschnozzle tool heads
    Firmware files included (at no extra cost!)
        No more hunting around for the correct files or having to compile it yourself
    View temperature graph in full screen when printing
    Transition software management platform (including issue tracking)
        Swap to using Phabricator which is a Free Software alternative to GitHub
        Our installation is located here: https://code.alephobjects.com
        Some reasons why we think Phabricator is a better tool: http://phabricator.org/comparison/

## Fixed

    No more useless error messages in print window console
    If your printer loses power, print jobs are canceled instead of restarted
    Cannot click movement buttons mid print (because it sucks to accidentally ruin a perfectly good print)
    Canceling a print disables motors and cools down the bed and nozzle(s)
        Closing the print window also does the same
    [Fedora] endless loop bug squished
    Correct bed size for TAZ
        Prints will now be correctly centered
        Items that completely covered the build surface used to fall off the right side

## Quickprint Profiles

    Standardize profile names

New Name	Old Name
Standard	Normal Print / Normal Quality
High speed	Low Quality / Fast Print
High detail	High Quality
High clarity	(did not exist previously)
High strength	(did not exist previously)

    New Mini profiles:
        V2 LulzBot Hexagon Flexystruder

    New TAZ profiles:
        V2 LulzBot Hexagon tool heads
            FlexyStruder
            Dual Extruder
            FlexyDually
        V1 Budaschnozzle tool heads
            FlexyStruder
            Dual Extruder
            FlexyDually

# v16

Publicly released: never `乁ʕ •̀ ۝ •́ ʔㄏ`

Swapped to a different version numbering system (major.minor). One "dummy" build was made but not released as no new features were added. Old system was based on Ultimaker's upstream version (year.month.buildnumber), dash (-), then LulzBot version number (major.minor). It is no longer possible to confuse downstream with an upstream version number. AWESOME!


# v15.02.1-1.03

Publicly released: 2015-06-15 `(∩╹□╹∩)`

Based off Ultimaker's 15.02.01 version of Cura
## New

    Support for 0.35mm and 0.5mm nozzle sizes on Taz 5
    Printer head size numbers for LulzBot Mini
        Allows sequential "one-at-a-time" printing
        These fields used to contain zeroes
    Combing (in expert config) now has three states instead of just being on/off
        New option is called "No Skin" which will skips retractions in certain conditions
    Single/Multiple layer view toggling
    Perimeters can now be printed before or after infill

## Fixed

    When importing a profile, any missing values are set to defaults

# v14.09-1.18.8

Publicly released: 2015-02-17 `ᕙ[  ͒ ﹏ ͒  ]ᕗ`
This was the first release of Cura LulzBot Edition.

# License

This page is licensed under [CC BY-SA 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) Aleph Objects, Inc.