# Cura LulzBot Edition

While working for Aleph Objects, Inc. I (with much assistance from my trusty contractors) was in charge of maintaining this mixed Python/C++ application for 3D printing. It is derived from Cura, which was created by David Braam and Ultimaker https://github.com/daid/LegacyCura.

The software did the hard mathematical work to acutally make something with a 3D printer. It took a CAD model (typically in .STL format) along with specifications for printer and material as input. It then "sliced" the model into many layers and produced the .GCODE output which could be run by a printer.

Original repo hosted here https://code.alephobjects.com/diffusion/CURA. This software has been superseeded by Cura 2 which keeps many of the roots of this app but with a completely redesigned UI based on QT.

# Innovations
Allow me to toot my team's horn for a moment...  :postal_horn: More details available in the [release notes](release-notes.md) or just read the entire commit log. But who has time for that?

## Integration of print profiles (aka Quickprint)
![screenshot](screenshots/materials.png)

Enabled users to rapidly manufacture in a variety of materials. Previously, if you wanted to use a material other than the 3 defaults it was quite involved and messy. Decide the material, go to our website, find the correct file which combined printer x toolhead x material, download, open cura, import material, slice, print. There were plenty of steps to mess up and confuse! After our changes it was much simpler to pick from a dropdown list and hit print. See scripts/recreate_lulzbot_profiles.py and all the data in resources/quickprint.

## Categorizing materials by difficulty
![screenshot](screenshots/categories.png)

At some point there were so many materials to pick from, you couln't fit them all in a single dropdown menu. We decided to separate them by ease of printing. This provided hints on reliability when comparing several materials. Also includes an "experimental" category providing a starting point for materials that are known to fail in some conditions.

## Introduction of pause feature
Using a simple USB cable between printer and computer you could quickly queue up a job. It was very satisfying feeling to click the print button and immediately hear your printer come to life, obeying your every command like the good little robot that it is. :green_heart: However, if something went wrong (such as about to run out of filament) there was no way to halt the print. A typical job lasted between a few and 24 hours, so it could mean lots of wasted time and material. People had been requesting the ability to pause for ages and we (finally) gave it to them! It also allowed manually swap out filament color/material for some cool effects. :smile:

## Toolhead selection system
![screenshot](screenshots/toolheads.png)

Over time the capabilities of our printer's hardware grew and new materials were brought to market. Newer toolheads were developed that could cope with these newer materials. As a result we had to continue evolving our software to keep things easy to use. A simple "click the image" type configuration selection system was developed to great success. Additionally it allowed indicating which materials are compatible with the currently installed toolhead.
